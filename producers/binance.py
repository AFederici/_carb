import json from kafka
import KafkaClient from kafka
import SimpleProducer from kafka
import KafkaProducer
import requests
import time

#binance
#rate limite = 1200 / min = 20 / sec

def lambda_handler(event, context):
    if 'pair' in event: topic = event['pair']
    else: return '<?xml version=\"1.0\" encoding=\"UTF-8\"?>'\
           '<Response><Message>No Trade Pair</Message></Response>'
    endPoint = "https://api.binance.com/api/v3/bookTicker/price?symbol="+topic #BTCUSDT
	print("Received event: " + str(event))
    kafka = KafkaClient("XXXX.XXX.XX.XX:XXXX")
    print(kafka)
    producer = SimpleProducer(kafka, async = False)
    print(producer)
    m = {'bid': -1, 'ask': -1, 'time': -1, 'ex':'binance'}
    ms_rate = 0.05
    m['time'] = time.time()
    endTime = m['time'] + (10 * 60)
    while m['time'] < endTime:
    	subprocess.run(["sleep", str(ms_rate)])
    	response = requests.get(endPoint).json()
    	if 'symbol' not in response: return "smybol is invalid"
    	m['time'] = time.time()
    	m['bid'] = response['bidPrice']
        m['ask'] = response['askPrice']
	    producer.send_messages(topic,json.dumps(m).encode('utf-8'))
	    print(producer.send_messages)
