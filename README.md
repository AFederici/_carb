# Electronic Trading project

![Architecture](https://gitlab.com/AFederici/_carb/-/raw/master/img/SysDesign.png)

The two main components that are using facing is a webserver as well as an orchestration server. Starting simpler, the webserver (Flask perhaps) simply fetches from a database (S3) and visualizes the data with some frontent (React perhaps). Additionally, it can be added where users can type in various queries to get specific DB results. The orchestration server is what will manage AWS - there are auto timeouts to our AWS workers so the main reason for this server is to act as a scheduler that can continue running but also discontinue workers. Although this seems more like a negative - the reasoning for this design will be touched upon later.  The idea is that a user can send a POST request to the server \{Operation:(start/stop), Exchanges:\[], Currencies:\[]} to configure the system. In addition, there should be some logic in place here (possibly in XML) with rate limits, valid pairs, etc for each exchange (as well as valid exchanges of course).  These services could be set up to run locally or on an EC2 instance. Neither should be particularly resource intensive.  Whether in EC2 or locally, a docker file should be set up to make portability easy. Docker files can be stored in Amazon's container registry service if desired.

Moving on, a API gateway is set up to manage our user facing servers with AWS. This will route and serve DB requests as well as handle starting/stopping workers. There could be seperate API's but at the end of the day it isn't necessary - the orchestration server will likely not have many requests/second.

In order to actually do something useful in real-time, the final component is Kafka. While there is some use to doing a bunch of data scraping -> storing -> post-analysis, real-time processing is ideal.  The design here is to create a collection of lambda functions that will create a massively distributed and scalable system. Similar projects often have a central server connecting to many exchanges - with hundreds of exchanges, hundreds of currencies, and potentially wanting mutliple instances of the same (exchange, currency) nodes, this just isn't feasible to run on a local server. You will be heavily subject to rate limiting and likely can't completely parallelize so many network requests.

"AWS Lambda provides easy scaling and high availability to your application code without any effort or responsibility of you managing and provisioning EC2 instances. With Amazon EC2 you are responsible for provisioning capacity, monitoring fleet health and performance, and designing for fault tolerance and scalability.
With AWS Lambda, you pay only for what you use. You are charged based on the number of requests for your functions and the time your code executes. The Lambda free tier includes 1M free requests per month and 400,000 GB-seconds of compute time per month. A no brainer…"

These instances will be split up into producers and consumers. For each exchange, there will be a decoder/encoder that can request data from the corresponding API and put it in some standardized format. The consumer is much more general - it simply needs to do some time based aggregation on its assigned topic - the implementaiton isn't really fleshed out. Something like aggregate accross a currency along a one second window and return low/high price, low/high volume, etc and store to DB. Because we can do encoding on the producer side, there only need be one consumer definition, but there can potentially be 1 per topic.

Kafka cluster is automatically managed by AWS, and the creation of these lambda instances is managed by out scheduler server. There is a timeout on these of 15 minutes, so you could do something simple like loop for 10 minutes and just reschedule the instance every 9 minutes.

Overall, AWS will allow for a very powerful architecture that will allow for arbitrage analysis, even across a single exchange. This is interesting because some exchanges are cloud based themselves and we may be able to see arbitrage across these cloud instances for the same exchange - something that woudln't be possible to analyze without an architecture like this that will be able to avoid rate limiting


**_[Useful References](https://gitlab.com/AFederici/_carb/-/wikis/Useful-Links)_**

**_[1B+ daily vol exchanges](https://coinmarketcap.com/rankings/exchanges/)_**

- Venus,
- Binance,
- Zg.com
- Huobi global,
- Upbit,
- Hbtc,
- Vcc exchange,
- OKEx,
- Dsdaq,
- Mexo exchange,
- Xtheta global,
- blitz,
- bitforex,
- bidesk,
- idcm,
- bione,
- hotcoin global,
- tokencan,
- HitBTC,
- ZT,
- HCoin,
- Ecxx,
- CoinBene,
- RightBTC,
- Changelly,
- Bitribe,
- Coinbase,
- AOFEX,
- Bitthumb,
- WBF exchange,
- ZB.com,
- digiFinex,
- PayBito,
- Gitbget,
- Kucoin,
- Kraken,
- Biki,
- Coinsbit,
- UPEX,
- DCoin,
- IndoEx,
- FTX,
- EXX,
- WhiteBIT,
- Bitfinex,
- CoinTiger,
- Paribu,
- Coinbit,
- BItrue,
- Biconomy,
- BKEX,
- XT,
- LongBit,
- MXC.COM,
- Coinone,
- DragonEX,
- Bitstamp,
- OMGfin
